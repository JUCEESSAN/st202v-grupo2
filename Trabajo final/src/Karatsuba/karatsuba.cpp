//============================================================================
// Name        : Karatsuba.cpp
// Author      : Leonardo Zambrano
// Version     : 2.0
// Copyright   : ZXDe�
// Description : Funci�n Karatsuba
//============================================================================

#include <iostream>
using namespace std;

int elevar(unsigned long long x, unsigned long long y){
	if (y == 0){
		return 1;
	}else if (y == 1){
		return x;
	}else{
		return x * elevar(x, y-1);
	}
}

int primeros(unsigned long long digitos, unsigned long long &numero){
	return numero/elevar(10, digitos);
}

int ultimos(unsigned long long digitos, unsigned long long &numero){
	return numero % elevar(10, digitos);
}

int maximo(unsigned long long u,unsigned long long v){
	if (u>=v){
		return u;
	}else{
	return v;
	}
}

int digitos (unsigned long long n, unsigned long long &dig){
	if (n < 10){
	return dig+1;
	}else{
		dig++;
		return digitos(n/10, dig);
	}
}

unsigned long long Karatsuba (unsigned long long &n1, unsigned long long &n2){
	unsigned long long dig1=0, dig2=0;
	unsigned long long numDigitos = maximo(digitos(n1, dig1), digitos(n2, dig2));
	if (numDigitos <= 1){
		return n1*n2;
	}
	numDigitos = (numDigitos / 2) + (numDigitos % 2);
	unsigned long long a;
	unsigned long long b;
	unsigned long long c;
	unsigned long long d;
	unsigned long long P1;
	unsigned long long P2;
	unsigned long long Suma1;
	unsigned long long Suma2;
	unsigned long long P3;
	unsigned long long Total;
	a = primeros(numDigitos, n1);
	b = ultimos(numDigitos, n1);
	c = primeros(numDigitos, n2);
	d = ultimos(numDigitos, n2);
	P1=Karatsuba(a, c);
	P2=Karatsuba(b, d);
	Suma1 = a + b;
	Suma2 = d + c;
	P3=Karatsuba(Suma1, Suma2);
	Total=elevar(10, 2*numDigitos)*P1+elevar(10, numDigitos)*(P3-P1-P2)+P2;
	return Total;
}

