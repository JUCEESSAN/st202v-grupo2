/*
 * karatsuba.hpp
 *
 *  Created on: 11 abr. 2018
 *      Author: Pedro Marroqu�n
 */

#ifndef KARATSUBA_KARATSUBA_HPP_
#define KARATSUBA_KARATSUBA_HPP_


unsigned long long Karatsuba (unsigned long long &n1, unsigned long long &n2);


#endif /* KARATSUBA_KARATSUBA_HPP_ */
