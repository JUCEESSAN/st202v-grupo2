#include <windows.h>
#include <iostream>
#include <vector>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include <values.h>
#include <fstream>
#include <conio.h>
#include "Karatsuba/karatsuba.hpp"
#include "Fisher/fisher.hpp"
#include "Dijkstra/dijkstra.hpp"
#include "BBS/bbs.hpp"
using namespace std;
struct Problemas{
	int nProblema;
	string Problema;
	string claves[4];
	string respuesta;
};

struct Alumno{
	string Apellido;
	string Codigo;
	unsigned long long Contrasenia;
};
struct Profesor{
	string Apellido;
	string NombreCl;
	unsigned long long Contrasenia;
};
vector <struct Alumno> ListaA;
vector<struct Profesor> ListaB;

bool Validacionusuar(string r, vector<struct Profesor> ListaB){
	bool valid=false;
	for(int i=0;i<ListaB.size();i++){
		if(r==ListaB[i].NombreCl){
			valid=true;
			break;
		}
	}
	return valid;
}

bool Validacioncod(string r, vector<struct Alumno> ListaA){
	bool valid=false;
	for(int i=0;i<ListaA.size();i++){
		if(r==ListaA[i].Codigo){
			valid=true;
			break;
		}
	}

	return valid;
}

bool ValidacionContraP(unsigned long long r, vector<struct Profesor> ListaB){
	bool valid=false;
	for(int i=0;i<ListaB.size();i++){
		if(r==ListaB[i].Contrasenia){
			valid=true;
			break;
		}
	}
	return valid;
}
bool ValidacionContraA(unsigned long long r, vector<struct Alumno> ListaA){
	bool valid=false;
	for(int i=0;i<ListaA.size();i++){
		if(r==ListaA[i].Contrasenia){
			valid=true;
			break;
		}
	}
	return valid;
}
int main() {
	struct Problemas listaP[30]={1,"¿Cuál es el Teorema de Pitagoras?","a=b+c","a^2=b^2+c^2","a^3=b^3+c^3","a^4=b^4+c^4","a^2=b^2+c^2",
			2,"¿Cómo se halla el volumen(V) de un cilindro de revolución?","V=πgr^2","V=4πr/3","V=4πr^3","V=4πrg^2","V=πgr^2",
			3,"¿Cuánto es la cuarta parte de la tercera parte de 12?","3","2","1","6","1",
			4,"Si tengo 20 metros de cinta roja y debo partirlos en 40 lazos, ¿cuántos centimetros debe medir el lazo?(m)","0.5","0.2","1.0","1.5","0.5",
			5,"¿Cuánto es 2018-1998?","20","42","21","19","20",
			6,"¿Cuánto es el 25% de 400?","100","200","300","400","100",
			7,"Si un edificio mide 80 metros más la mitad de lo que mide, ¿cuánto mide el edificio?","160","80","120","60","160",
			8,"Si hay 25 patos y 5 perros, ¿Cuántas se pueden contar?","12","70","60","55","70",
			9,"Si tengo 15 litros de agua y regalo 3, ¿Cuántos litros de agua tengo?","9","5","6","7","9",
			10,"Un jugador de basket hizo 27 canastas y erró 23, ¿Cuál fue el porcentaje de lanzamientos buenos?","54%","45%","60%","42%","54%",
			11,"¿Cuántas hras hay en 5 días?","40","240","120","420","120",
			12,"¿Cuál es el factorial de 5?","120","201","100","42","120",
			13,"¿A cuánto equivale el coseno de 0°?","1","0","-1","1/2","1",
			14,"¿Cuál es el promedio de 12, 15, 9?","12","15","18","9","12",
			15,"¿A cuánto equivale π?","3.141598","4.131598","3.241421","1.12465","3.141598",
			16,"¿A cuánnto equivale e?","2.718282","3.141598","3.152565","5.445361","2.718282",
			17,"¿Cuánto es la raíz de 2500?","50","25","500","125","50",
			18,"¿Cuánto equivale un mega?","10^6","10^3","10^9","10^3","10^6",
			19,"¿Cuánto es ln(e)?","1","e","0","-1","1",
			20,"La suma de los n primeros números enteros es:","n(n+1)/2","n(n-1)/2","n^2/2","(n-1)(n+1)/2","n(n+1)/2",
			21,"¿A cuánto equivale el seno de 0°?","1","0","-1","0.5","0",
			22,"¿Cuál es el término 6 de la sucesión de Fibonacci?","8","9","13","15","8",
			23,"Si el radio de una circunferencia mide 10, ¿Cuánto equivale su área?","200π","400π","100π","200","200π",
			24,"¿Cuántos lados octágono?","4","6","8","10","8",
			25,"¿Cuánto grados mide un ángulo en un triángulo equilátero?","60°","120°","30°","90°","60°",
			26,"¿Cuánto es 6 elevado al cuadrado?","49","36","100","68","36",
			27,"¿Cuántos km recorrerá un carro en tres horas si va 50Km/h?","100","300","150","500","150",
			28,"¿Cuántos metros tiene un kilometro?","1","100","1000","50","1000",
			29,"Si ayer fue Martes,¿Qué día será el día siguiente de antes de ayer de hoy?","Martes","Jueves","Viernes","Lunes","Martes",
			30,"¿Cuántos grados mide un ángulo recto?","90°","180°","0°","45°","90°"};
int number;
struct Alumno EstructuraA;
struct Profesor EstructuraB;
EstructuraA={"Zambrano","20170186B",375162};
ListaA.push_back(EstructuraA);
EstructuraA={"Marroquin","20170025I",50425};
ListaA.push_back(EstructuraA);
EstructuraA={"Alcala","20170046F",92782};
ListaA.push_back(EstructuraA);
EstructuraA={"Escalon","20172044K",4122748};
ListaA.push_back(EstructuraA);
EstructuraB={"Caballero","jbrcaballero",375162};
ListaB.push_back(EstructuraB);
EstructuraB={"Alcantara","dan.alcantara",50425};
ListaB.push_back(EstructuraB);
EstructuraB={"Chung-Ching","rchungc",92782};
ListaB.push_back(EstructuraB);
EstructuraB={"Acosta","rAcosta",4122748};
ListaB.push_back(EstructuraB);
int n;
	cout<<"Bienvenido al programa del grupo 02"<<endl;
	cout<<"Por favor;ingrese el numero correspondiente a la operacion que desea realizar"<<endl;
	cout<<"Si quiere ver la ejecucion del algoritmo de Karatsuba ingrese (1)"<<endl;
	cout<<"Si quiere ver la ejecucion del algoritmo de Fisher-Yates ingrese (2)"<<endl;
	cout<<"Si quiere ver la ejecucion del algoritmo de Blum Blum Shup ingrese (3)"<<endl;
	cout<<"Si quiere ver la ejecucion del algoritmo de Dijkstra ingrese (4)"<<endl;
	cout<<"Si quiere ver la ejecucion del trabajo grupal ingrese (5)"<<endl;
	cin>>n;
	switch(n){
	case 1:
		unsigned long long n1;
		unsigned long long n2;
		cout<<"Ingrese el primer numero que desee multiplicar";cin>>n1;
		cout<<"Ingrese el segundo numero que desee multiplicar";cin>>n2;
		cout<<"La multiplicacion de sus numeros usando el metodo de Karatsuba es: "<<Karatsuba (n1, n2)<<endl;
		system("cls");
		Sleep(2000);
		main();
		break;
	case 2:
		int cant;
		cout<<"Ingrese la cantidad de elementos que desee ordenar aleatoriamente..."<<endl; cin>>cant;
		int numeros[100];
		cout<<"Ingrese los elementos que desee ordenar aleatoriamente..."<<endl;
		for(int i=0; i<cant; i++){
			cin>>numeros[i];
		}
		fisherYates(numeros, cant);
		cout<<"Algoritmo de Fisher..."<<endl;
		for(int i=0; i<cant; i++){
			cout<<numeros[i]<<" ";
		}
		system("cls");
		Sleep(2000);
		main();
		break;

	case 3:
		cout<<"Algoritmo BBS"<<endl;
		GENERADOR();
		system("cls");
		Sleep(2000);
		main();
		break;
	case 4:
		cout<<"Algoritmo de Dijkstra (demo)..."<<endl;
		Sleep(2000);
		dijkstra();
		system("cls");
		Sleep(2000);
		main();
		break;
	case 5:
		system("cls");
		char usuar;
		int datobusca;
		cout << "Bienvenido a Examinator"<<endl;
		cout<<"Por favor; ingrese si es Profesor(P) o Alumno (A)"; cin>>usuar;
		if(usuar=='P'){
			string nusuar;
			cout<<"Ingrese su usuario"<<endl;cin>>nusuar;
			if(Validacionusuar(nusuar, ListaB)==1){
				unsigned long long contra1;
				cout<<"Ingrese su contraseña"<<endl;cin>>contra1;
				if(ValidacionContraP(contra1,ListaB)==1){
					cout<<"Bienvenido profesor"<<endl;
					cout<<"¿Qué es lo que desea hacer?"<<endl;
					cout<<"Ingresar problemas(1)\n";
					cout<<"Buscar preguntas(2)\n";
					cout<<"Imprimir preguntas(3)";
					cin>>number;
					switch(number){
					case 1 : cout<<"En este momento no está disponible entrada de preguntas, vuelva más tarde"; break;
					case 2 :{ cout<<"Ingrese el número de pregunta que desea buscar";
								cin>>datobusca;
								for(int p=0; p<30; p++){
									if(datobusca == listaP[p].nProblema){
										cout<<listaP[p].Problema;
										for(int q=0; q<4;q++){
											cout<<listaP[p].claves[q]<<endl;
										}
									}else{
										cout<<"Numero no encontrado";
									}
								}
								break;
					case 3 :
						for(int f=0;f<30; f++){
							cout<<listaP[f].nProblema<<". "<<listaP[f].Problema<<endl;
							for(int g=0;g<4;g++){
								cout<<(f+1)<<". "<<listaP[f].claves[g]<< endl;
							}
							cout<<"Respuesta: "<<listaP[f].respuesta<<endl;
						}
						break;
					default: cout<<"Error al cargar...";break;
					}
					}
				}else{
					cout<<"Contraseña Incorrecta"<<endl;
				}
		}else{
			char afir;
				cout<<"Disculpe, usted no tiene una cuenta aquí"<<endl;
				cout<<"¿Desea crear una cuenta? S/N";cin>>afir;
				if(afir=='S'){
					string a;
					string b;
					unsigned long long na1=rand();
					unsigned long long na2=rand();
					unsigned long long c;
					struct Profesor Profesor1;
					cout<<"Ingrese su apellido";cin>>a;
					cout<<"Ingrese un nombre clave que desee sin espacios";cin>>b;
					c=Karatsuba (na1, na2);
					Profesor1={a,b,c};
					cout<<"Su contraseña es: "<<c<<endl;
					ListaB.insert(ListaB.begin()+0,Profesor1);
				cout<<"Bienvenido profesor"<<endl;
				cout<<"¿Qué es lo que desea hacer?"<<endl;
									cout<<"Ingresar problemas(1)\n";
									cout<<"Buscar preguntas(2)\n";
									cout<<"Imprimir preguntas(3)";
									cin>>number;
									switch(number){
									case 1 : cout<<"En este momento no está disponible entrada de preguntas, vuelva más tarde"; break;
									case 2 :{ cout<<"Ingrese el número de pregunta que desea buscar";
												cin>>datobusca;
												for(int p=0; p<30; p++){
													if(datobusca == listaP[p].nProblema){
														cout<<listaP[p].Problema;
														for(int q=0; q<4;q++){
															cout<<listaP[p].claves[q]<<endl;
														}
													}else{
														cout<<"Numero no encontrado";
													}
												}
												break;
									case 3 :
										for(int f=0;f<30; f++){
											cout<<listaP[f].nProblema<<". "<<listaP[f].Problema<<endl;
											for(int g=0;g<4;g++){
												cout<<(f+1)<<". "<<listaP[f].claves[g]<< endl;
											}
											cout<<"Respuesta: "<<listaP[f].respuesta<<endl;
										}
										break;
									default: cout<<"Error al cargar...";break;
									}
									}
								}else{
									cout<<"Contraseña Incorrecta"<<endl;
								}
		}

		}else if(usuar=='A'){
			string nusuar2;
			string respuesta;
			cout<<"Ingrese su código UNI"<<endl;cin>>nusuar2;
			if(Validacioncod(nusuar2, ListaA)==1){
				unsigned long long contra2;
				char examinator;
				cout<<"Ingrese su contraseña"<<endl;cin>>contra2;
				if(ValidacionContraA(contra2,ListaA)==1){
					cout<<"Bienvenido alumno"<<endl;
					cout<<"¿Qué es lo que desea hacer?"<<endl;
					do{
					cout<<"¿Comenzar el test?(Y/N)";cin>>examinator;
					if(examinator=='Y'){
						for(int w=0;w<5;w++){
							cout<<listaP[w].Problema<<endl;
							for(int t=0;t<4;t++){
								cout<<listaP[w].claves[t]<<endl;
							}
							cout<<"Ingrese clave correcta:";cin>>respuesta;
							if(respuesta==listaP[w].respuesta){
								cout<<"Es correcta";
							}else{
								cout<<"Incorrecto, la respuesta era: "<<listaP[w].respuesta;
							}
						}
						
						
					}
				}while(examinator=='N');
				}else{
					cout<<"Contraseña Incorrecta"<<endl;
				}
			}else{
				char afir;
				cout<<"Disculpe;usted no tiene una cuenta aquí"<<endl;
				cout<<"¿Desea crear una cuenta? S/N";cin>>afir;
				if(afir=='S'){
					string ap;
					string codA;
					unsigned long long x;
					unsigned long long y;
					unsigned long long w;
					struct Alumno Alumno1;
					cout<<"Ingrese su apellido";cin>>ap;
					cout<<"Ingrese su código UNI";cin>>codA;
					x=(codA[0]-'0')*1000+(codA[1]-'0')*100+(codA[2]-'0')*10+(codA[3]-'0');
					y=(codA[4]-'0')*1000+(codA[5]-'0')*100+(codA[6]-'0')*10+(codA[7]-'0');
					w=Karatsuba (x, y);
					cout<<"Su contraseña es: "<<w<<endl;
					Alumno1={ap,codA,w};
					ListaA.insert(ListaA.begin()+1,Alumno1);
					cout<<"Bienvenido alumno"<<endl;
					cout<<"¿Qué es lo que desea hacer?"<<endl;
										do{
										cout<<"¿Comenzar el test?(Y/N)";cin>>examinator;
										if(examinator=='Y'){
											for(int w=0;w<5;w++){
												cout<<listaP[w].Problema<<endl;
												for(int t=0;t<4;t++){
													cout<<listaP[w].claves[t]<<endl;
												}
												cout<<"Ingrese clave correcta:";cin>>respuesta;
												if(respuesta==listaP[w].respuesta){
													cout<<"Es correcta";
												}else{
													cout<<"Incorrecto, la respuesta era: "<<listaP[w].respuesta;
												}
											}
											
											
										}
									}while(examinator=='N');

					}
				}

		}

		break;

	}

	return 0;
}


